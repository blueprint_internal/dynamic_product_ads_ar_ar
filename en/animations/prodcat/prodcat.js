function ProductCatalog(resources) {
    ProductCatalog.resources = resources;

    popUpArray = [];
    graphics = null

    bizC = null;

    biz1L = null;
    biz2L = null;
    biz3L = null;
    biz4L = null;

    biz1R = null;
    biz2R = null;
    biz3R = null;
    biz4R = null;

    topStyle = null;
    popTextStyle = null;

    icoText0 = null;

    icoText1L = null;
    icoText2L = null;
    icoText3L = null;
    icoText4L = null;

    icoText1R = null;
    icoText2R = null;
    icoText3R = null;
    icoText4R = null;

    yLoc0 = 40;
    yLoc1 = 120;
    yLoc2 = 340;
    yLoc3 = 560;
    yLoc4 = 780;

    leftX = 220;
    rightX = 580;


    game = null;

    maskN = null;
    rect = null;
    rectSize = null;

    rectSizeX = 450;

    rectSizeY = 280; 

    bizObjArray = [];

    roundedRectCurve = 5;
    brightBlueBoxes = "#4080FF";

    closebtn = null;

}


ProductCatalog.prototype = {

    init: function() {
        game = new Phaser.Game(800, 1000, Phaser.CANVAS, 'ProductCatalog', {
            preload: this.preload,
            create: this.create,
            parent: this
        });

    },

    preload: function() {

        game.scale.maxWidth = 800;
        game.scale.maxHeight = 1000;
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.stage.backgroundColor = '#FFFFFF'
        game.load.image('ico1L', ProductCatalog.resources.ico1L);
        game.load.image('ico2L', ProductCatalog.resources.ico2L);
        game.load.image('ico3L', ProductCatalog.resources.ico3L);
        game.load.image('ico4L', ProductCatalog.resources.ico4L);
        game.load.image('ico1R', ProductCatalog.resources.ico1R);
        game.load.image('ico2R', ProductCatalog.resources.ico2R);
        game.load.image('ico3R', ProductCatalog.resources.ico3R);
        game.load.image('ico4R', ProductCatalog.resources.ico4R);
        game.load.image('closebtn', ProductCatalog.resources.closebtn);

    },

    create: function(evt) {

        this.parent.makeFlow();

    },

    makeFlow: function() {

        topStyle = {
            font: "bold 35px freight-sans-pro",
            fill: "#FFFFFF",
            wordWrap: true,
            wordWrapWidth: 500,
            align: "center"
        };

        //game.input.touch.preventDefault = true;

        graphics = game.add.graphics(10, 10);
        graphics.lineStyle(8, 0x4E5664, 1);

        rectSize = 300;

        bizC = game.add.sprite(400, yLoc0);
        bizC.anchor.set(0.5);

        var rndRectC = game.add.graphics(0, 0);
        rndRectC.beginFill(0x4080FF); //"#5890FF",
        rndRectC.drawRoundedRect(-335, -35, 675, 65, roundedRectCurve)

        bizC.addChild(rndRectC);
        icoText0 = game.add.text(0, 0, ProductCatalog.resources.ico0text, topStyle);
        icoText0.anchor.set(0.5);
        bizC.addChild(icoText0);

        //COLUMN 1
        icoText1L = ProductCatalog.resources.ico1text;
        icoText2L = ProductCatalog.resources.ico2text;
        icoText3L = ProductCatalog.resources.ico3text;
        icoText4L = ProductCatalog.resources.ico4text;

       

        biz1L = this.makeNewBiz(leftX, yLoc1, 'ico1L', icoText1L);
        biz2L = this.makeNewBiz(leftX, yLoc2, 'ico2L', icoText2L);
        biz3L = this.makeNewBiz(leftX, yLoc3, 'ico3L', icoText3L);
        biz4L = this.makeNewBiz(leftX, yLoc4, 'ico4L', icoText4L);

        graphics.lineStyle(8, 0x4080FF, 1);
        this.lineStart(bizC);
        this.lineEnd(biz1L);
        graphics.lineStyle(10, 0x4080FF, 1);
        this.lineEnd(biz2L);
        this.lineEnd(biz3L);
        this.lineEnd(biz4L);

        // COLUMN 2
        icoText1R = ProductCatalog.resources.ico1text;
        icoText2R = ProductCatalog.resources.ico2text;
        icoText3R = ProductCatalog.resources.ico3text;
        icoText4R = ProductCatalog.resources.ico4text;

        biz1R = this.makeNewBiz(rightX, yLoc1, 'ico1R', icoText1R);
        biz2R = this.makeNewBiz(rightX, yLoc2, 'ico2R', icoText2R);
        biz3R = this.makeNewBiz(rightX, yLoc3, 'ico3R', icoText3R);
        biz4R = this.makeNewBiz(rightX, yLoc4, 'ico4R', icoText4R);

        graphics.lineStyle(8, 0x4080FF, 1);
        this.lineStart(bizC);
        this.lineEnd(biz1R);
        graphics.lineStyle(10, 0x4080FF, 1);
        this.lineEnd(biz2R);
        this.lineEnd(biz3R);
        this.lineEnd(biz4R)

        //BUTTONS
        bizC.inputEnabled = true;

        biz1L.inputEnabled = true;
        biz2L.inputEnabled = true;
        biz3L.inputEnabled = true;
        biz4L.inputEnabled = true;

        biz1R.inputEnabled = true;
        biz2R.inputEnabled = true;
        biz3R.inputEnabled = true;
        biz4R.inputEnabled = true;


        bizC.events.onInputUp.add(this.actionOnClick, this);
        bizC.id = "0";

        //bizC.events.onInputUp.add(this.actionOnClick, {
            //param1: "0"
        ///});

        //bizC.events.onInputUp.add(this.clickListener, {
          //  param1: "0"
        //});

        biz1L.events.onInputUp.add(this.actionOnClick, this);
        biz1L.id = "1";

        biz2L.events.onInputUp.add(this.actionOnClick, this);
        biz2L.id = "2";

        biz3L.events.onInputUp.add(this.actionOnClick, this);
        biz3L.id = "3";

        biz4L.events.onInputUp.add(this.actionOnClick, this);
        biz4L.id =  "4";

        biz1R.events.onInputUp.add(this.actionOnClick, this);
        biz1R.id =  "1";

        biz2R.events.onInputUp.add(this.actionOnClick, this);
        biz2R.id =  "2";

        biz3R.events.onInputUp.add(this.actionOnClick, this);
        biz3R.id =  "3";
        
        biz4R.events.onInputUp.add(this.actionOnClick, this);
        biz4R.id = "4";

    },

    lineStart: function(obj) {
        graphics.moveTo(obj.x, obj.y);
    },

    lineEnd: function(obj) {
        graphics.lineTo(obj.x, obj.y);
    },

    actionOnClick: function(evt){

        console.log("params" + evt.param1);

        var resourceText  = ProductCatalog.resources["pop"+evt.id];

        var icoText  = ProductCatalog.resources["ico"+evt.id+"text"];

        this.trigger("domPopup",{title:icoText,body:resourceText});

    },
    

    makeNewBiz: function(xLoc, yLoc, icoArtZ, icoTextN) {

        var biz1LN = game.add.sprite(xLoc, yLoc);
        var rndRect1 = game.add.graphics(0, 0);
        var biz1LNart = game.add.sprite(0, 20, icoArtZ);
        var icoText1L = game.add.text(0, 240, icoTextN, topStyle);

        //FORMAT SCALE
        biz1LN.scale.setTo(.70, .70);
        biz1LNart.scale.setTo(1.1, 1.1);
        rndRect1.beginFill(0x4080FF);
        rndRect1.drawRoundedRect(-(rectSizeX / 2), 0, rectSizeX, rectSizeY, roundedRectCurve);

        //ANCHOR
        biz1LN.anchor.set(.5, 0);
        biz1LNart.anchor.set(.5, 0);
        icoText1L.anchor.set(.5);

        //ADD CHILDREN
        biz1LN.addChild(rndRect1);
        biz1LN.addChild(biz1LNart);
        biz1LN.addChild(icoText1L);
        bizObjArray.push(biz1LN);

        return biz1LN;

    },

    clickListener: function() {
/*
        ///this.trigger("domPopup",{title:ProductCatalog.resources["pop"+,body:ProductCatalog.resources.ico2text});

        if (popUpArray.length > 0) {

            for (i = 0; i < popUpArray.length; i++) {
                var object2Destroy = popUpArray[i];
                object2Destroy.destroy();
            }

        }

        var xLoc = null;
        var yLoc = null;
        var icoArtZ = null;
        var icoTextN = null;

        switch (this.param1) {
            case "0":
                xLoc = 40;
                yLoc = 10;
                icoArtZ = '';
                icoTextN = ProductCatalog.resources.pop0;
                break;
            case "1":
                xLoc = 40;
                yLoc = 100;
                icoArtZ = 'ico1L';
                icoTextN = ProductCatalog.resources.pop1;
                break;
            case "2l":
                xLoc = 40;
                yLoc = 300;
                icoArtZ = 'ico2L'
                icoTextN = ProductCatalog.resources.pop2
                break;
            case "3l":
                xLoc = 40;
                yLoc = 500;
                icoArtZ = 'ico3L'
                icoTextN = ProductCatalog.resources.pop3
                break;
            case "4l":
                xLoc = 40;
                yLoc = 700;
                icoArtZ = 'ico4L'
                icoTextN = ProductCatalog.resources.pop4
                break;
            case "2r":
                xLoc = 40;
                yLoc = 300;
                icoArtZ = 'ico2R'
                icoTextN = ProductCatalog.resources.pop2
                break;
            case "3r":
                xLoc = 40;
                yLoc = 500;
                icoArtZ = 'ico3R'
                icoTextN = ProductCatalog.resources.pop3
                break;
            case "4r":
                xLoc = 40;
                yLoc = 700;
                icoArtZ = 'ico4R'
                icoTextN = ProductCatalog.resources.pop4
                break;
            default:

        };


        popTextStyle = {
            font: "bold 20px freight-sans-pro",
            fill: "#000000",
            wordWrap: true,
            wordWrapWidth: 420,
            align: "left"
        };

        popTextStyle2 = {
            font: "bold 20px freight-sans-pro",
            fill: "#000000",
            wordWrap: true,
            wordWrapWidth: 700,
            align: "left"
        };

        var popUpBox = game.add.sprite(xLoc, yLoc);
        var rndRect1 = game.add.graphics(0, 0);
        rndRect1.beginFill(0xdddddd);

        if (this.param1 == 0) {

            rndRect1.drawRoundedRect(0, 0, 750, 220, roundedRectCurve)
            popUpBox.addChild(rndRect1);

            var icoText1L = game.add.text(20, 60, icoTextN, popTextStyle2);

        } else {

            rndRect1.drawRoundedRect(0, 0, 750, 320, roundedRectCurve)
            popUpBox.addChild(rndRect1);

            var popUpBoxart = game.add.sprite(20, 35, icoArtZ);
            popUpBox.addChild(popUpBoxart);
            var icoText1L = game.add.text(310, 30, icoTextN, popTextStyle);

        }

        var closeX = game.add.text(722, 7, "X", popTextStyle);

        popUpBox.addChild(icoText1L);
        popUpBox.addChild(closeX);

        bizObjArray.push(popUpBox);

        popUpBox.inputEnabled = true;
        //Good Destroy method
        popUpBox.events.onInputUp.add(function() { popUpBox.destroy() }, this);

        popUpArray.push(popUpBox)

        return popUpBox;
        */
    }



}